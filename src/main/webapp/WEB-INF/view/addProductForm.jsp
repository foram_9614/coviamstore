<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Product</title>
</head>
<body>
<h4>Add Product</h4>
	<form:form action="addProduct" commandName="product">
		<table>

			<tr>
				<td>productName</td>
				<td><form:input path="productName" /></td>
				<td><form:errors path="productName" /></td>
			</tr>

			<tr>
				<td>productCategory</td>
				<td><form:input path="productCategory" /></td>
				<td><form:errors path="productCategory" /></td>
			</tr>

			<tr>
				<td>productPrice</td>
				<td><form:input path="productPrice" /></td>
				<td><form:errors path="productPrice" /></td>
			</tr>
			
			<tr>
				<td>productQuantity</td>
				<td><form:input path="productQuantity" /></td>
				<td><form:errors path="productQuantity" /></td>
			</tr>
			
			

			<tr>
				<td><input type="submit" value="Add Product"></td>
			</tr>
		</table>

	</form:form>
	

</body>
</html>