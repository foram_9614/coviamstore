package com.store.productstore.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.store.entity.vo.ProductVO;
import com.store.productstore.dao.ProductDAO;
import com.store.productstore.entity.Product;

@Service
@Transactional
public class IProductService implements ProductService {

	@Autowired
	ProductDAO productDao;

	@Override
	public List<ProductVO> search(String productCategory, String productName) {
		// TODO Auto-generated method stub
		List<Product> products = productDao.findByProductCategoryStartingWithIgnoreCaseOrProductNameContainingIgnoreCase(productCategory, productName);
		List<ProductVO> productVOs = new ArrayList<>();
		for (Product product : products) {
			ProductVO productVO = new ProductVO();
			productVO.setProductCategory(product.getProductCategory());
			productVO.setProductID(product.getProductID());
			productVO.setProductName(product.getProductName());
			productVO.setProductPrice(product.getProductPrice());
			productVOs.add(productVO);
		}
		return productVOs;
	}

	@Override
	public Product checkProduct(int productID) {
		// TODO Auto-generated method stub
		return productDao.findOne(productID);
	}

	@Override
	public List<ProductVO> getAllProducts() {
		List<Product> products = (List<Product>) productDao.findAll();
		List<ProductVO> productVOs = new ArrayList<>();
		ProductVO productVO;
		for (Product product : products) {
			productVO = new ProductVO();
			productVO.setProductCategory(product.getProductCategory());
			productVO.setProductID(product.getProductID());
			productVO.setProductName(product.getProductName());
			productVO.setProductPrice(product.getProductPrice());
			productVOs.add(productVO);
		}
		return productVOs;
	}

	public void addProduct(Product product) {
		// TODO Auto-generated method stub
		productDao.save(product);
	}

	@Override
	public void buyProduct(int productId, int quantity) {
		// TODO Auto-generated method stub
		Product product = productDao.findOne(productId);
		int productQuantity = product.getProductQuantity();
		product.setProductQuantity(productQuantity - quantity);
		productDao.save(product);

	}

	@Override
	public List<ProductVO> searchByName(String productName) {
		// TODO Auto-generated method stub
		List<Product> products = productDao.findByProductNameContainingIgnoreCase(productName);
		List<ProductVO> productVOs = new ArrayList<>();
		for (Product product : products) {
			ProductVO productVO = new ProductVO();
			productVO.setProductCategory(product.getProductCategory());
			productVO.setProductID(product.getProductID());
			productVO.setProductName(product.getProductName());
			productVO.setProductPrice(product.getProductPrice());
			productVOs.add(productVO);
		}
		return productVOs;
	}

}
