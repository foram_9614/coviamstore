package com.store.productstore.service;

import java.util.List;

import com.store.entity.vo.ProductVO;
import com.store.productstore.entity.Product;


public interface ProductService {
	

	List<ProductVO> getAllProducts();

	
	List<ProductVO> search(String productCategory, String productName);
	
	Product checkProduct(int productID);
	
	void buyProduct(int productId, int quantity);


	void addProduct(Product product);


	List<ProductVO> searchByName(String productName);
	

}
