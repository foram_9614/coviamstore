package com.store.productstore.dao;

import org.springframework.stereotype.Repository;

import com.store.productstore.entity.Product;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

@Repository
public interface ProductDAO extends CrudRepository<Product,Integer>{
	
	public List<Product> findByProductCategory(String productCategory);

	public List<Product> findByProductName(String productName);
	
	public List<Product> findByProductCategoryStartingWithIgnoreCaseOrProductNameContainingIgnoreCase(String productCategory, String productName);
	
	public List<Product> findByProductNameContainingIgnoreCase(String productName);
	

}
