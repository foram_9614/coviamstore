package com.store.productstore.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.store.entity.vo.CartItems;
import com.store.entity.vo.OrderItems;
import com.store.entity.vo.ProductVO;
import com.store.productstore.entity.Product;
import com.store.productstore.service.ProductService;

@RestController
public class ProductController {
    
    private static String cartController = "http://localhost:8083/";
    
    @Autowired
    ProductService productService;
    
    @RequestMapping("/search/{productCategory}")
    public List<ProductVO> searchProduct(@PathVariable("productCategory") String productCategory){
        System.out.println(productCategory);
        List<ProductVO> productVOs = productService.search(productCategory, productCategory);
        
        return productVOs;
    }
    
    @RequestMapping("/searchByProduct/{productName}")
    public List<ProductVO> searchByName(@PathVariable("productName") String productName){
        List<ProductVO> productVOs = productService.searchByName(productName);
        return productVOs;
    }
    
    @RequestMapping("/addProductForm")
    public String addProductForm(Model model){
        Product product = new Product();
        model.addAttribute("product", product);
        return "addProductForm";
    }
    
    @RequestMapping("/addProduct")
    public String addProduct(@ModelAttribute("product") Product product){
        productService.addProduct(product);
        return "success";
    }
    
    @RequestMapping("/getAllProducts")
    public List<ProductVO> getProducts(){
        List<ProductVO> productList = productService.getAllProducts();
        if (productList.size() > 0) {
        	  Collections.sort(productList, new Comparator<ProductVO>() {
        	      @Override
        	      public int compare(ProductVO product1, ProductVO product2) {
        	    	  if(product1.getProductCategory().equals(product2.getProductCategory()))
        	    	  {
        	    		  return Double.compare(product1.getProductPrice(), product2.getProductPrice());
        	    	  }
        	          return product1.getProductCategory().compareTo(product2.getProductCategory());
        	      }
        	  });
        	}
        
        return productList;
    }
    
    @RequestMapping("/checkQuantity/{productId}/{quantity}")
    public boolean checkProduct(@PathVariable("productId") Integer productID, @PathVariable("quantity") Integer quantity){
        Product product = productService.checkProduct(productID);
        if(product.getProductQuantity()>=quantity&& quantity>0){
            return true;
        }
        return false;
    }
    
    @RequestMapping("/getProductById/{productId}")
    public ProductVO getProduct(@PathVariable("productId") Integer productID){
        Product product = productService.checkProduct(productID);
        ProductVO productVO = new ProductVO();
        productVO.setProductCategory(product.getProductCategory());
        productVO.setProductID(product.getProductID());
        productVO.setProductName(product.getProductName());
        productVO.setProductPrice(product.getProductPrice());
        return productVO;
    }
    
    @RequestMapping(value = "/buyProduct/{emailId}", method = RequestMethod.POST,  headers = "Accept=application/json")
    public void buyProduct(@RequestBody CartItems[] cart, @PathVariable("emailId") String emailId){
        
        for (CartItems cartItems : cart) {
            int productID = cartItems.getProductId();
            int quantity = cartItems.getQuantity();
            productService.buyProduct(productID, quantity);    
        }    
        //return true;
    }

}